#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include "calc.h"
#include "emu.h"

#define CONFIG      "FOV"
#define HELP_FILE   "./help"
#define DIR_PREFIX  "./"
#define CONFIG_EXT  ".cfg"
#define SLEEP_TIME  50
#define BUFFER_SIZE 256
#define PMPT_STR     "FOV Helper> "
#define PMPT_INVALID "\nInvalid entry.\n"
#define PMPT_SENS    "Sensitivity: "
#define PMPT_MV      "Match Value: "
#define PMPT_CFOV    "Current FOV: "
#define PMPT_VFOV    "Vertical FOV: "
#define PMPT_HFOV    "Horizontal FOV: "
#define PMPT_NFOV    "New FOV: "
#define PMPT_W       "Width: "
#define PMPT_H       "Height: "
#define PMPT_CM360   "CM/360: "
#define PMPT_DOM     "Degree Of Movement: "
#define PMPT_SZO     "Size Of Object: "
#define PMPT_DSTO    "Distance From Object: "
#define PMPT_PM      "Physical Movement: "
#define PMPT_DM      "Degrees Moved: "
#define PMPT_CM1     "CM/360 at 1 sensitivity: "
#define PMPT_CS      "Current Sensitivity: "
#define CMD_ERROR    "Invalid command.\n"
#define SET_ERROR    "Invalid setting: %s\n"
#define SET_SUCCESS    "%s set to %s\n"
#define FNF_ERROR      "Help file not found.\n"
#define CONFIG_SUCCESS "%s config file loaded successfully.\n"
#define CONFIG_ERROR   "Unable to load config file: %s\n"

void *Prompt(const char *p);
emu_settings LoadSettings(const char *file_name);
BOOL PrintHelp(const char *file_name);
void CleanupExit(int num, ...);
void FreeMultiple(int num, ...);
double PromptConvertSens();
emu_settings SetCommand(char *config, char *input, emu_settings s);
char *EvalAnswer(char *answer, emu_settings *s, HANDLE *t, char *buffer, 
                 char *string);
int TokenizeInput(const char* input, const char* delim, 
                  char tokens[][100], int maxTokens);


int main(int argc, char **argv) 
{
        HANDLE thread       = NULL;
        emu_settings values = LoadSettings(CONFIG);

        if (values.error_flag == 1) {
                printf(CONFIG_ERROR, CONFIG);
                values = (emu_settings) {0.0, 0.0, 0.0, 0.0, 50, 1, 0.0, 0.0, 
                                         0, 0, 0, VK_DELETE, VK_HOME, 0};
        }
        else
                printf(CONFIG_SUCCESS, CONFIG);

        while (true) {
                char *answer = Prompt(PMPT_STR);

                if (answer == NULL)
                        return EXIT_FAILURE;

                char *buffer = calloc(BUFFER_SIZE, sizeof(*buffer));

                if (buffer == NULL)
                        CleanupExit(2, EXIT_FAILURE, answer);

                char *string = calloc(BUFFER_SIZE, sizeof(*string));

                if (string == NULL)
                        CleanupExit(3, EXIT_FAILURE, answer, buffer);

                char *eval = EvalAnswer(answer, &values, &thread, buffer, 
                                        string);
                if (eval == NULL)
                        CleanupExit(4, EXIT_FAILURE, buffer, string, answer);

                printf("%s", eval);

                if(strcmp("exit", answer) == 0)
                        CleanupExit(4, EXIT_SUCCESS, buffer, string, answer);

                FreeMultiple(3, buffer, string, answer);

                Sleep(SLEEP_TIME);
        }

        return EXIT_SUCCESS;
}

void CleanupExit(int num, ...)
{
        va_list valist;

        va_start(valist, num);

        int exit_code = va_arg(valist, int);
  
        for (int i = 1; i < num; i++) {
                void *ptr = va_arg(valist, void*);
                free(ptr);
        }
  
        va_end(valist);

        exit(exit_code);
}

void FreeMultiple(int num, ...) 
{
        va_list valist;

        va_start(valist, num);
  
        for (int i = 0; i < num; i++) {
                void *ptr = va_arg(valist, void*);
                free(ptr);
        }
  
        va_end(valist);
}

void *Prompt(const char *p)
{
        printf("%s", p);

        char *answer = calloc(BUFFER_SIZE, sizeof(char));

        if (answer == NULL)
                exit(EXIT_FAILURE);

        while(!fgets(answer, BUFFER_SIZE, stdin)) {
                printf(PMPT_INVALID);
                printf("%s", p);
        }

        answer[strcspn(answer, "\n")] = 0;

        return answer;
}

emu_settings LoadSettings(const char *file_name)
{
        emu_settings s;

        s.error_flag = 0;

        char new_name[BUFFER_SIZE];

        strcpy(new_name, DIR_PREFIX);
        strcat(new_name, file_name);
        strcat(new_name, CONFIG_EXT);

        FILE* file = fopen(new_name, "r");

        if(file == NULL) {
                s.error_flag = 1;
                return s;
        }
    
        char line[BUFFER_SIZE] = {0};

        while (fgets(line, sizeof(line), file)) {
                sscanf(line,"cm360=%lf",&s.cm360);
                sscanf(line,"dpi=%lf",&s.dpi);
                sscanf(line,"current_fov=%lf",&s.current_fov);
                sscanf(line,"new_fov=%lf",&s.new_fov);
                sscanf(line,"parts=%lf",&s.parts);
                sscanf(line,"match_value=%lf",&s.match_value);
                sscanf(line,"helper_fov=%lf",&s.helper_fov);
                sscanf(line,"sensitivity=%lf",&s.sensitivity);
                sscanf(line,"to_fov_key=%i",&s.to_fov);
                sscanf(line,"static_at_one=%lf",&s.static_at_one);
                sscanf(line,"calc_movement_key=%i",&s.calc_movement);
                sscanf(line,"match_mode=%i",&s.match_mode);
                sscanf(line,"helper_mode=%i",&s.helper_mode);
        }

        fclose(file);

        return s;
}

BOOL PrintHelp(const char *file_name)
{
        FILE* file = fopen(file_name, "r");

        if(file == NULL)
                return false;

        printf("\n");

        char line[BUFFER_SIZE] = {0};

        while (fgets(line, sizeof(line), file)) 
                printf("%s", line);

        printf("\n");

        fclose(file);

        return true;
}

int TokenizeInput(const char* input, const char* delim, 
                  char tokens[][100], int maxTokens) 
{
        char tempInput[100];  
        strcpy(tempInput, input);

        char* token    = strtok(tempInput, delim);  
        int tokenCount = 0;

        while (token != NULL && tokenCount < maxTokens) {
                strcpy(tokens[tokenCount], token);
                tokenCount++;
                token = strtok(NULL, delim);
        }

        return tokenCount;  
}

emu_settings SetCommand(char *config, char *input, emu_settings s)
{
        struct config_list {
                char *setting;
                char *format;
                void *target;
        };

        struct config_list cl[] = {
                {"cm360", "%lf", &s.cm360},
                {"dpi", "%lf", &s.dpi},
                {"current_fov", "%lf", &s.current_fov},
                {"new_fov","%lf", &s.new_fov},
                {"parts","%lf", &s.parts},
                {"match_value","%lf", &s.match_value},
                {"helper_fov","%lf", &s.helper_fov},
                {"sensitivity","%lf", &s.sensitivity},
                {"static_at_one", "%lf", &s.static_at_one},
                {"to_fov_key", "%i", &s.to_fov},
                {"calc_movement_key", "%i", &s.calc_movement},
                {"match_mode", "%i", &s.match_mode},
                {"helper_mode", "%i", &s.helper_mode},
        };

        double d_val = 0;
        int i_val    = 0;
        int is_set   = 0;
        s.error_flag = 0;

        for (int i = 0; i < sizeof(cl) / sizeof(cl[0]); i++) { 
                if(!strcmp(cl[i].setting, config)) {
                        if(!strcmp(cl[i].format, "%lf")) {
                                d_val = atof(input);
                                *((double*)cl[i].target) = d_val;
                                is_set = 1;
                                break;
                        }

                        if(!strcmp(cl[i].format, "%i")) {
                                i_val = atoi(input);
                                *((int*)cl[i].target) = i_val;
                                is_set = 1;
                                break;
                        }
                }
                       
        }

        if (is_set == 0)
                s.error_flag = 1;

        return s;

}


double PromptConvertSens()
{
        double sens = atof(Prompt(PMPT_SENS));
        double mv   = atof(Prompt(PMPT_MV));
        double cf   = atof(Prompt(PMPT_CFOV));
        double nf   = atof(Prompt(PMPT_NFOV));

        return CalcConvertSens(sens, nf, cf, mv);
}


char *EvalAnswer(char *answer, emu_settings *s, HANDLE *t, char *buffer, 
                 char *string)
{
        int max_tokens = 10;
        char tokens[max_tokens][100];

        int token_count = TokenizeInput(answer, " ", tokens, max_tokens);
        if (token_count == 0)
                return CMD_ERROR;
        
        if (!strcmp("set", tokens[0])) {
                *s = SetCommand(tokens[1], tokens[2], *s);
                if (s->error_flag) 
                        sprintf(string,SET_ERROR, tokens[1]);
                else
                        sprintf(string,SET_SUCCESS, tokens[1], tokens[2]);
                        
                return string;
        }
                        
        if(!strcmp("help", answer) || !strcmp("?", answer)) {
                BOOL h = PrintHelp(HELP_FILE);
                if (h == false)
                        return FNF_ERROR;
                return "";
        }

        if(sscanf(answer,"load %s", buffer)) {
                *s = LoadSettings(buffer);
                if (s->error_flag == 1)
                        printf(CONFIG_ERROR, buffer);
                else
                        printf(CONFIG_SUCCESS, buffer);
                return "";
        }

        if(!strcmp("exit", answer)) 
                return "exit";

        if(!strcmp("clear counter", answer)) {
                s->unit_counter = 0;
                return "\nCounter cleared.\n";
        }

        if(!strcmp("settings", answer)) {
                EmuPrintSettings(*s);
                return "";
        }

        if(!strcmp("helper on", answer)) {
                if (*t == NULL) {
                        *t = CreateThread(NULL, 0, EmuMain, s, 0, NULL);
                        return "Helper started.\n";
                }
                else 
                        sprintf(string, "Helper already started!\n");

                return string;
        }

        if(!strcmp("helper off", answer)) {
                if (*t == NULL)
                        sprintf(string, "Helper has not been started!\n");
                else {
                        if(TerminateThread(*t, 0) > 0) 
                                *t = NULL;

                        sprintf(string, "Helper stopped.\n");
                }
                return string;
        }

        if(!strcmp("convert sens", answer)) {
                double result = PromptConvertSens();

                if (s->static_at_one > 0) {
                        double cm = CalcSensToCm360(s->static_at_one, result);
                        sprintf(string, "\n%lf or %lf cm/360\n\n", result, cm);
                } else
                        sprintf(string, "\n%lf\n\n", result);

                return string;
        }

        if(!strcmp("convert cm360", answer)) {

                double cm360  = atof(Prompt(PMPT_CM360));
                double cf     = atof(Prompt(PMPT_CFOV));
                double nf     = atof(Prompt(PMPT_NFOV));
                double result = CalcConvertCm360(cm360, nf, cf);

                sprintf(string, "\n%lf cm/360 \n\n", result);

                return string;
        }

        if(!strcmp("calc movement", answer))
        {
                double wcm = atof(Prompt(PMPT_CM360));
                double fov = atof(Prompt(PMPT_DOM));
                double mcm = CalcDegreeToMovement(wcm, fov);

                sprintf(string, "\n%f cm\n\n", mcm);

                return string;
        }

        if(!strcmp("calc hfov", answer))
        {
                double vfov   = atof(Prompt(PMPT_VFOV));
                double width  = atof(Prompt(PMPT_W));
                double height = atof(Prompt(PMPT_H));
                
                sprintf(string, "\nHorizontal FOV: %f\n\n", 
                        CalcHFOV(vfov, width, height));

                return string;
        }

        if(!strcmp("calc vfov", answer))
        {
                double hfov   = atof(Prompt(PMPT_HFOV));
                double width  = atof(Prompt(PMPT_W));                
                double height = atof(Prompt(PMPT_H));
                double vfov   = CalcVFOV(hfov, width, height);

                sprintf(string, "\nVertical FOV: %f\n\n", vfov);

                return string;
        }

        if(!strcmp("calc degree_dist", answer))
        {
                double size     = atof(Prompt(PMPT_SZO));
                double distance = atof(Prompt(PMPT_DSTO));
                double d        = CalcDegreeDist(distance, size);

                sprintf(string, "\n%f degrees\n\n", d);

                return string;
        }

        if(!strcmp("calc degree", answer))
        {
                double wcm = atof(Prompt(PMPT_PM));
                double fov = atof(Prompt(PMPT_DM));
                double d   = CalcMovementToDegree(wcm, fov);

                sprintf(string, "\n%f cm/360\n\n", d);

                return string;
        }

        if(!strcmp("calc cm360", answer))
        {
                double cm   = atof(Prompt(PMPT_CM1));
                double sens = atof(Prompt(PMPT_CS));
                double s    = CalcSensToCm360(cm, sens);

                sprintf(string, "\n%f cm/360\n\n", s);

                return string;
        }

        return "Invalid command.\n";
}
