#include <math.h>
#include "calc.h"

double CalcVFOV(double hfov, double width, double height)
{
    double ratio   = height / width;
    double hfovRad = CalcRadian(hfov);
    double vfovRad = 2 * atan(tan(hfovRad / 2) * ratio);

    return CalcDegree(vfovRad);
}

double CalcHFOV(double vfov, double width, double height)
{
    double ratio   = width / height;
    double vfovRad = CalcRadian(vfov);
    double hfovRad = 2 * atan(tan(vfovRad / 2) * ratio);

    return CalcDegree(hfovRad);
}

double CalcDistFOV(double screen_width, double view_dist)
{
    double fov = 2 * atan(screen_width / (2 * view_dist));
    
    return CalcDegree(fov);
}

double GetCmPerDegree(double cm, double degree)
{
    return cm / degree;
}

double CalcMovementToDegree(double cm, double degree)
{
    return GetCmPerDegree(cm, degree) * CIRCLE;
}

double CalcDegreeToMovement(double cm360, double degree)
{
    return GetCmPerDegree(cm360, CIRCLE) * degree;
}

double CalcSensToCm360(double static_at_one, double sensitivity)
{
    return static_at_one / sensitivity;
}

double CalcRadian(double degree)
{
    return (degree * M_PI) / HALF_CIRCLE;
}

double CalcDegree(double radian)
{
    return (radian * 180) / M_PI;
}

double CalcConvertSens(double sensitivity, double new_fov, double current_fov, 
                       double match_value)
{
    current_fov = CalcRadian(current_fov / 2);
    new_fov     = CalcRadian(new_fov / 2);

    double f_dividend = atan(match_value * tan(new_fov));
    double f_divisor  = atan(match_value * tan(current_fov));
    double factor     = f_dividend / f_divisor ;

    return sensitivity * factor;
}

double CalcConvertCm360(double cm360, double new_fov, double current_fov)
{
    double movement = CalcDegreeToMovement(cm360, current_fov);

    return CalcMovementToDegree(movement, new_fov);
}

double CalcDegreeDist(double distance, double size)
{
    double size_dist = size / (2 * distance);
    double angle     = 2 * atan(size_dist);

    return angle * (HUMAN_FOV / M_PI);
}
