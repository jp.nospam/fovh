#ifndef CALC_H
#define CALC_H

#include <math.h>

#define HALF_CIRCLE 180
#define HUMAN_FOV   210
#define CIRCLE      360

/* Find amount of physical mouse movement needed to reach a given degree
 * based on cm/360 
 */
double CalcDegreeToMovement(double cm360, double degree);

/* Find cm/360 based on the amount of physical movement to a degree */
double CalcMovementToDegree(double cm, double degree);

/* In-game Sensitivity to CM/360 */
double CalcSensToCm360(double static_at_one, double sensitivity);

/* Convert degrees to Radians */
double CalcRadian(double degree);

double CalcDegree(double radian);

/* Converts in-game sensitivity to the new FOV */
double CalcConvertSens(double sensitivity, double new_fov, double current_fov, 
                       double match_value);
                             
double CalcConvertCm360(double cm360, double new_fov, double current_fov);

double GetCmPerDegree(double cm, double degree);

/* Calculate the amount of degrees in FOV an object takes up from a distance */
double CalcDegreeDist(double distance, double size);

double CalcHFOV(double vfov, double width, double height);

double CalcVFOV(double hfov, double width, double height);

double CalcDistFOV(double screen_width, double view_dist);

#endif
